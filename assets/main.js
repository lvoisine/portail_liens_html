function showTitle(id){
  titre = document.getElementById('cardTitle');

  switch (id) {
    case "bdd":
      titre.textContent="Base de données source";
      break;
    case "accinfo":
      titre.textContent="Fragilité à l’accès à l’information";
      break;
    case "accinter":
      titre.textContent="Fragilité à l’accès aux interfaces numériques";
      break;
    case "accglobal":
      titre.textContent="Fragilité à l’accès";
      break;
    case "compadm":
      titre.textContent="Fragilité en compétences administratives";
      break;
    case "compnum":
      titre.textContent="Fragilité en compétences numériques";
      break;
    case "compglobal":
      titre.textContent="Fragilité en compétences";
      break;
    case "global":
      titre.textContent="Fragilité numérique globale structurée en 2 parties définies autour de 4 axes";
      break;
    default:
      titre.textContent="";
  }
}

function unshowTitle(){
  document.getElementById('cardTitle').textContent="";
}
